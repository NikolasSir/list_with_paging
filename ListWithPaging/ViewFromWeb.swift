//
//  ViewFromWeb.swift
//  ListWithPaging
//
//  Created by N Sh on 23.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI
import WebKit

struct ViewFromWeb: UIViewRepresentable {
    
    let request: URLRequest
    
    func makeUIView(context: UIViewRepresentableContext<ViewFromWeb>) -> WKWebView{
        return WKWebView()
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.load(request)
    }
}

struct ViewFromWeb_Previews: PreviewProvider {
    static var previews: some View {
        ViewFromWeb(request: URLRequest(url: URL(string: "")!))
    }
}
