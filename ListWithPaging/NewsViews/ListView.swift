//
//  ListView.swift
//  ListWithPaging
//
//  Created by N Sh on 22.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI
import NetworkServices

struct ListView: View {
    @EnvironmentObject var newsViewModel: NewsListViewModel
    var onRowAppear: ((News) -> Void)
    var searchString: String
    
    init(searchString: String,
         onInited: () -> Void,
         onRowAppear: @escaping (News) -> Void
         ){
        self.onRowAppear = onRowAppear
        onInited()
        self.searchString = searchString
    }
    
    var body: some View {
        return NavigationView{
            List(self.newsViewModel.news.filter{ self.searchString.isEmpty ? true: $0.contains(search: searchString)}){ item in
                NavigationLink(destination: NewsRowDetails(item: item)){
                    NewsRow(item: item,
                            searchString: self.searchString)
                    .onAppear{
                        self.onRowAppear(item)
                    }
                }
            }
            .navigationBarTitle("News")
        }
    }
}

