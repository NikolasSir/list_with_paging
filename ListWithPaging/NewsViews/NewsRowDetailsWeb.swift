//
//  NewsRowDetailsWeb.swift
//  ListWithPaging
//
//  Created by N Sh on 25.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI

struct NewsRowDetailsWeb: View {
    @EnvironmentObject var newsListModel: NewsListViewModel
    var urlString: String?

    var body: some View {
            ViewFromWeb(request: self.newsListModel.loadNewsDetails(urlString: urlString))
        }
}

