//
//  NewsRowDetails.swift
//  ListWithPaging
//
//  Created by N Sh on 22.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation
import SwiftUI
import NetworkServices

struct NewsRowDetails: View {
    @EnvironmentObject var newsListModel: NewsListViewModel
    let item: News
    
    var body: some View{
        VStack(alignment: .center){
            Text(item.author ?? "")
                .font(.largeTitle)
            Text(item.description ?? "")
                .font(.headline)
                .padding()
            NavigationLink(destination: NewsRowDetailsWeb(urlString: item.url!)){
                Text("Details")
            }
        }
    }
}
