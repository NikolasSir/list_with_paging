//
//  NewsRow.swift
//  ListWithPaging
//
//  Created by N Sh on 22.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI
import UIKit
import NetworkServices

struct NewsRow: View {
    @EnvironmentObject var newsListModel: NewsListViewModel
    
    private struct CombinedText: View{
        private var firstString: String
        private var search: String
        private var secondString: String
        private var font: Font?
        private var additionalColor: Color?
        private var color: Color?
        
        init(firstString: String,
             search: String,
             secondString: String,
             font: Font,
             additionalColor: Color,
             color: Color){
            self.firstString = firstString
            self.search = search
            self.secondString = secondString
            self.font = font
            self.additionalColor = additionalColor
            self.color = color
        }
        
        var body: some View{
            Text(firstString)
                .font(font)
                .foregroundColor(additionalColor)
            +
            Text(search)
                .font(font)
                .foregroundColor(color)
            +
            Text(secondString)
                .font(font)
                .foregroundColor(additionalColor)
        }
    }
    
    let item: News
    var searchString: String
    
    init(item: News, searchString: String){
        self.item = item
        self.searchString = searchString
    }
    
    var body: some View{
        HStack{
            VStack(alignment: .leading){
                    if (item.author?.contains(searchString) ?? false){
                        CombinedText(
                            firstString: item.author?.getHighlightedString(searchString: searchString).firstPart ?? "",
                            search: item.author?.getHighlightedString(searchString: searchString).searchString ?? "",
                            secondString: item.author?.getHighlightedString(searchString: searchString).secondPart ?? "",
                            font: .headline,
                            additionalColor: .black,
                            color: .yellow)
                    } else {
                        Text(item.author ?? "")
                            .font(.headline)
                    }
                    if (item.title?.contains(searchString) ?? false){
                        CombinedText(
                            firstString: item.title?.getHighlightedString(searchString: searchString).firstPart ?? "",
                            search: item.title?.getHighlightedString(searchString: searchString).searchString ?? "",
                            secondString: item.title?.getHighlightedString(searchString: searchString).secondPart ?? "",
                            font: .largeTitle,
                            additionalColor: .black,
                            color: .yellow)
                    } else {
                        Text(item.title ?? "")
                            .font(.largeTitle)
                    }
                if (item.description?.contains(searchString) ?? false){
                    CombinedText(
                        firstString: item.description?.getHighlightedString(searchString: searchString).firstPart ?? "",
                        search: item.description?.getHighlightedString(searchString: searchString).searchString ?? "",
                        secondString: item.description?.getHighlightedString(searchString: searchString).secondPart ?? "",
                        font: .callout,
                        additionalColor: .gray,
                        color: .yellow)
                } else {
                    Text(item.description ?? "")
                        .font(.callout)
                        .foregroundColor(.gray)
                }
                
                if self.newsListModel.isPageLoading &&
                    self.newsListModel.news.isLast(item){
                    Divider()
                    Text("Loading...")
                }
            }
        }
    }
}
