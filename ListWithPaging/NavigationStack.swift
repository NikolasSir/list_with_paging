//
//  NavigationStack.swift
//  ListWithPaging
//
//  Created by N Sh on 30.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI

struct NavigationStack<Content: View>: View {
    let content: Content
    
    init(@ViewBuilder content: () -> Content){
        self.content = content()
    }
    
    var body: some View {
        Text("Text")
    }
}

/*struct NavigationStack_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack()
    }
} */
