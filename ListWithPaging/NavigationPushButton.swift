//
//  NavigationPushButton.swift
//  ListWithPaging
//
//  Created by N Sh on 01.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI

struct NavigationPushButton<Destination, Button>: View where Button: View, Destination: View {
    let button: Button
    let destination: Destination
    
    init(@ViewBuilder destination: @escaping () -> Destination, @ViewBuilder button: @escaping() -> Button){
        self.button = button()
        self.destination = destination()
    }
    
    var body: some View{
        Text("Hi")
    }
}

/*struct NavigationPushButton_Previews: PreviewProvider {
    static var previews: some View {
        NavigationPushButton()
    }
} */
