//
//  ContentView.swift
//  ListWithPaging
//
//  Created by N Sh on 12.03.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI
import NetworkServices

struct ContentView: View {
    @EnvironmentObject var newsListModel: NewsListViewModel
    @State private var selectorIndex = 0
    @State private var searchTitle = ""
    @State private var titles = [String]()
    @State private var searchWord = ""
    
    var body: some View {
        
        VStack(alignment: .center){
            HStack(alignment: .center){
                TextField("What do you want to search?", text: self.$searchTitle)
                    .fixedSize()
                    Button(action: {
                        self.titles.append(self.searchTitle)
                        self.selectorIndex = self.titles.count - 1
                    }){
                        Image(systemName: "plus.app")
                    }
                    .padding()
                    Button(action: {
                        if (self.selectorIndex != self.titles.count - 1){
                            self.titles.removeLast()
                        }
                    }){
                        Image(systemName: "paintbrush")
                    }
                    .padding()
                    Button(action: {
                        self.titles.removeAll()
                    }){
                        Image(systemName: "trash")
                    }
                    .padding()
            }
            if (!titles.isEmpty){
                Section(header: Text("What's new?")){
                    Picker("News", selection: self.$selectorIndex){
                        ForEach(self.titles){ title in
                            Text(title).tag(self.titles.getIndex(title))
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                SearchBar(text: $searchWord)
                    .padding(.horizontal)
                ListView(searchString: self.searchWord,
                         onInited: {
                    self.newsListModel.loadPage(selectorIndex: self.selectorIndex, selectedString: self.titles[self.selectorIndex])
                }){ item in
                    if self.newsListModel.news.isLast(item) {
                        self.newsListModel.loadPage(selectorIndex: self.selectorIndex, selectedString: self.titles[self.selectorIndex])
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(NewsListViewModel(newsService: NewsService(), newsDetailsService: NewsDetailsService()))
    }
}
