//
//  NView.swift
//  ListWithPaging
//
//  Created by N Sh on 30.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI

struct NView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct NView_Previews: PreviewProvider {
    static var previews: some View {
        NView()
    }
}
