//
//  StringExt.swift
//  ListWithPaging
//
//  Created by N Sh on 01.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation
import UIKit

extension String{
    public func getHighlightedString(searchString: String)->(firstPart: String, searchString: String, secondPart: String){
        guard let range = self.range(of: searchString) else {
            return ("", "", "")
        }
        return (firstPart: String(self[..<range.lowerBound]),
                searchString: searchString,
                secondPart: String(self[range.upperBound...]))
    }
}
