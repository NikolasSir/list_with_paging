//
//  NewsExt.swift
//  ListWithPaging
//
//  Created by N Sh on 01.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation
import NetworkServices

extension News{
    public func contains(search: String)->Bool{
        return self.author?.contains(search) ?? false ||
            self.content?.contains(search) ?? false ||
            self.description?.contains(search) ?? false ||
            self.title?.contains(search) ?? false
    }
}
