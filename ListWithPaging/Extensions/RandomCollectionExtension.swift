//
//  RandomCollectionExtension.swift
//  ListWithPaging
//
//  Created by N Sh on 13.03.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation

extension String: Identifiable {
    public var id: String {
        self
    }
}

extension RandomAccessCollection where Self.Element: Identifiable {
    public func isLast(_ item: Element)->Bool {
        guard isEmpty == false else {
            return false
        }
        guard let itemIndex = lastIndex(where: { AnyHashable($0.id) == AnyHashable(item.id) })  else {
            return false
        }
        return distance(from: itemIndex, to: endIndex) == 1
    }
}

extension RandomAccessCollection where Self.Element: Equatable {
    public func getIndex(_ item: Element) -> Int {
        for pair in self.enumerated(){
            if pair.element == item {
                return pair.offset
            }
        }
        return -1
    }
}
