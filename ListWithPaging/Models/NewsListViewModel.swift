//
//  NewsListViewModel.swift
//  ListWithPaging
//
//  Created by N Sh on 21.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI
import NetworkServices

final class NewsListViewModel: ObservableObject {
    @Published private(set) var news:[News] = [News]()
    private(set) var page = 0
    private var newsService: NewsService
    private var newsDetailsService: NewsDetailsService
    private(set) var isPageLoading: Bool = false
    private(set) var counter = 0
    
    init(newsService: NewsService, newsDetailsService: NewsDetailsService){
        self.newsService = newsService
        self.newsDetailsService = newsDetailsService
    }
    
    func loadPage(selectorIndex: Int, selectedString: String){
        guard isPageLoading == false else {
            return
        }
        if (counter != selectorIndex){
            counter = selectorIndex
            page = 0
            news.removeAll()
        }
        isPageLoading = true
        page += 1
        newsService.loadNews(selectedString: selectedString, page: page){
            result in
            switch result{
            case .success(let news):
                self.news.append(contentsOf: news)
            case .failure(_):
                print("Network Error")
            }
            self.isPageLoading = false
        }
    }
    
    func loadNewsDetails(urlString: String?) -> URLRequest{
        return newsDetailsService.loadNewsDetails(urlString: urlString)
    }
}
