//
//  ServiceLocator.swift
//  ListWithPaging
//
//  Created by N Sh on 24.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation

final class ServiceLocator{
    private var registry: [String: Any] = [:]
    static var shared = ServiceLocator()
    
    private init(){
        
    }
    
    func registerService<T>(service: T){
        let key = "\(T.self)"
        registry[key] = service
    }
    
    func tryGetService<T>() -> T?{
        let key = "\(T.self)"
        return registry[key] as? T
    }
    
    func getService<T>() -> T{
        let key = "\(T.self)"
        return registry[key] as! T
    }
}
