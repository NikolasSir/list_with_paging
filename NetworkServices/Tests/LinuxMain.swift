import XCTest

import NetworkServicesTests

var tests = [XCTestCaseEntry]()
tests += NetworkServicesTests.allTests()
XCTMain(tests)
