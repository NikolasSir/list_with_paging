import XCTest
@testable import NetworkServices

final class NetworkServicesTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(NetworkServices().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
