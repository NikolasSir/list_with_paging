//
//  NewsDetailsService.swift
//  ListWithPaging
//
//  Created by N Sh on 24.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation

public class NewsDetailsService{
    
    private var google = "https://www.google.ru"
    
    public init(){
        
    }
    
    public func loadNewsDetails(urlString: String?) -> URLRequest{
        return URLRequest(url: (URL(string: urlString ?? google)!))
    }
}
