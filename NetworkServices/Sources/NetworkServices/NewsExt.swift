//
//  NewsExt.swift
//  ListWithPaging
//
//  Created by N Sh on 22.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation

extension News: Identifiable{
    public var id: String {
        self.title ?? UUID().uuidString
    }
}
