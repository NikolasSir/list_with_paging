//
//  NewsService.swift
//  ListWithPaging
//
//  Created by N Sh on 24.04.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation

public class NewsService{
    
    enum NetworkError: Error{
        case networkIssue
    }
    
    private let apiKey = "11e68152726149159c9a5137388c6849"
    
    public init(){
        
    }
    
    public func loadNews(selectedString: String,
                  page: Int,
                  completion: @escaping (Result<[News], Error>) -> Void){
        NewsAPI.getNews(apiKey: apiKey, q: selectedString, page: page){
            responce, error in
            if let articles = responce?.articles {
                completion(.success(articles))
            } else {
                completion(.failure(NetworkError.networkIssue))
            }
        }
    }
}
